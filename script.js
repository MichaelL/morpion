const statut = document.getElementById("statut"),
    victory = [
        // lignes
        [0,1,2],
        [3,4,5],
        [6,7,8],
        // colonnes
        [0,3,6],
        [1,4,7],
        [2,5,8],
        // diagonales
        [0,4,8],
        [2,4,6]
    ]

let game = true,
    restartButton = document.getElementById("restart"),
    player = "X",
    gameStatus = ["","","","","","","","",""],
    thisCase = document.getElementsByClassName("case")

const playerTurn = () => "C'est au tour de <b>"+player+"</b>",
    winner = () => "Le joueur <b>"+player+"</b> a gagné",
    equality = () => "Égalité"

statut.innerHTML = playerTurn()

document.querySelectorAll(".case").forEach(cell => cell.addEventListener("click", clickCase))

restartButton.hidden = true
restartButton.addEventListener("click", restart)

function clickCase() {
    const index = parseInt(this.dataset.index)

    if (gameStatus[index] !== "" || !game) {
        return
    }
    gameStatus[index] = player
    this.innerHTML = player
    
    ifYouWin()
}

function ifYouWin() {
    let youWin = false

    for (let condition of victory) {
        let one = gameStatus[condition[0]],
            two = gameStatus[condition[1]],
            three = gameStatus[condition[2]]

        if (one === "" || two === "" || three === "") {
            continue
        }
        if (one === two && two === three) {
            thisCase[condition[0]].style.backgroundColor = "#9ACD32"
            thisCase[condition[1]].style.backgroundColor = "#9ACD32"
            thisCase[condition[2]].style.backgroundColor = "#9ACD32"
            youWin = true
            break
        }
    }

    if (youWin) {
        statut.innerHTML = winner()
        game = false
        restartButton.hidden = false
        return
    }
    if (!gameStatus.includes("")) {
        statut.innerHTML = equality()
        game = false
        restartButton.hidden = false
        return
    }

    player = player === "X" ? "O" : "X"
    statut.innerHTML = playerTurn()
}

function restart() {
    player = "X"
    game = true
    gameStatus = ["","","","","","","","",""]
    statut.innerHTML = playerTurn()
    document.querySelectorAll(".case").forEach(function (cell) {
        cell.innerHTML = ""
        cell.style.backgroundColor = "#fff"
    })
    restartButton.hidden = true
}